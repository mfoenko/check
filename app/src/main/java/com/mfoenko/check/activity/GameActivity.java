package com.mfoenko.check.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfoenko.check.R;
import com.mfoenko.check.data.Player;
import com.mfoenko.check.data.PlayerGameEntity;
import com.mfoenko.check.data.PlayersDatabase;
import com.mfoenko.check.misc.MultiSelectAdapter;

import java.util.Arrays;

/**
 * Created by mfoenko on 9/4/15.
 */
public class GameActivity extends Activity implements ActionMode.Callback{


    public static final String PLAYERS_KEY = "players";
    private PlayerGameEntity[] mPlayers;
    private PlayersAdapter mPlayerAdapter;
    private MultiSelectAdapter mPayoutSelector;
    private PlayersDatabase mPlayersDatabase;

    private static final int[] WAGER_AMOUNTS = {50, 100, 200, 500};
    private int wagerAmount = 0;

    private TextView mWagerTV;
    private TextView mPotTV;
    private Button mPayoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Parcelable[]  playerParcelables;

        try {
           playerParcelables =  getIntent().getExtras().getParcelableArray(PLAYERS_KEY);
        }catch(NullPointerException e){
            return;
        }
        if(playerParcelables == null){
            return;
        }
        mPlayers = new PlayerGameEntity[playerParcelables.length];
        for(int i = 0;i<mPlayers.length;i++){
            mPlayers[i] = new PlayerGameEntity((Player)playerParcelables[i]);
        }
        setContentView(R.layout.activity_game);

        this.mWagerTV = (TextView)findViewById(R.id.wager_amount);
        this.mPotTV = (TextView)findViewById(R.id.pot_amount);
        this.mPayoutButton = (Button)findViewById(R.id.payout_button);

        updateWagerStats();

        this.mPlayerAdapter = new PlayersAdapter(mPlayers);
        RecyclerView playerRV = (RecyclerView)findViewById(R.id.players_recycler_view);
        playerRV.setAdapter(mPlayerAdapter);
        playerRV.setLayoutManager(new LinearLayoutManager(this));


        this.mPayoutSelector = new MultiSelectAdapter(mPlayers);


        ViewGroup wagerButtons = (ViewGroup)findViewById(R.id.wager_buttons);
        for(int i=0;i< WAGER_AMOUNTS.length;i++){
            Button wagerButton = (Button)wagerButtons.getChildAt(i);
            wagerButton.setText(String.valueOf(WAGER_AMOUNTS[i]));
            wagerButton.setTag(WAGER_AMOUNTS[i]);
        }

        mPlayersDatabase = PlayersDatabase.getInstance(this);
    }

    private void updateWagerStats(){
        mWagerTV.setText(String.valueOf(wagerAmount));
        mPotTV.setText(String.valueOf(getPotAmount()));
    }

    private void updatePayoutButton(){
        mPayoutButton.setEnabled(mPayoutActionMode == null);
    }

    private int getPotAmount(){
        int potAmount = 0;
        for(PlayerGameEntity p:mPlayers){
            potAmount += p.wager;
        }
        return potAmount;
    }


    public void onRaiseButtonPressed(View view){
        int amount = (int)view.getTag();
        wagerAmount += amount;
        updateWagerStats();
        enableActivePlayers();
    }

    public void onCallButtonPressed(View view){
        int pos = (int)((View)view.getParent()).getTag();
        mPlayers[pos].wager = wagerAmount;
        mPlayers[pos].hasActed = true;
        mPlayerAdapter.notifyItemChanged(pos);
        updateWagerStats();
    }

    public void onFoldButtonPressed(View view){
        int pos = (int)((View)view.getParent()).getTag();
        Log.i("AAA", mPlayers[pos] + " has folded");
        mPlayers[pos].isFolded = mPlayers[pos].hasActed = true;

        mPlayerAdapter.notifyItemChanged(pos);
    }

    private ActionMode mPayoutActionMode;

    public void onPayoutButtonPressed(View view){
        startActionMode(this);
    }

    public void payout(){
        int totalCash = 0;
        for(PlayerGameEntity p:mPlayers){
            totalCash += p.wager;
            p.cash -= p.wager;
            p.reset();
        }

        int num = mPayoutSelector.getNumSelected();

        for(int i=0;i<mPayoutSelector.selectedItems.length;i++){
            if(mPayoutSelector.selectedItems[i]){
                mPlayers[i].cash += totalCash/num;
            }
        }

        mPayoutSelector.clear();
        wagerAmount = 0;
        for(PlayerGameEntity p:mPlayers){
            mPlayersDatabase.updatePlayer(p);
        }

        mPlayerAdapter.notifyDataSetChanged();
        updateWagerStats();

        if(mPayoutActionMode != null){
            mPayoutActionMode.finish();
        }

    }



    public void enableActivePlayers(){
        for(PlayerGameEntity p:mPlayers){
            p.hasActed = p.isFolded;
        }
        mPlayerAdapter.notifyDataSetChanged();
    }

    public void resetAllPlayers(){
        for(PlayerGameEntity p:mPlayers){
            p.reset();
        }
        mPlayerAdapter.notifyDataSetChanged();
    }

    public void resetBoard(){
        resetAllPlayers();
        wagerAmount = 0;

    }

    private class PlayersAdapter extends RecyclerView.Adapter<PlayerVH>{

        private PlayerGameEntity[] players;

        public PlayersAdapter(PlayerGameEntity[] players){
            this.players = players;
        }

        @Override
        public PlayerVH onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.player_game_list_item, parent, false);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    if (mPayoutActionMode != null) {
                        mPayoutSelector.selectedItems[position] = !mPayoutSelector.selectedItems[position];
                        notifyItemChanged(position);
                    }
                }
            });

                    return new PlayerVH(itemView);
        }

        @Override
        public void onBindViewHolder(PlayerVH holder, final int position) {
            Log.e("AAA", Arrays.toString(players));



            PlayerGameEntity player = players[position];
            holder.itemView.setTag(position);
            holder.nameTV.setText(player.name);
            holder.cashTV.setText(String.valueOf(player.cash - player.wager));

            holder.foldBT.setEnabled(!player.isFolded);
            holder.foldBT.setVisibility(mPayoutActionMode == null ? View.VISIBLE : View.INVISIBLE);
            holder.callBT.setEnabled(!player.isFolded && player.wager < wagerAmount);
            holder.callBT.setVisibility(mPayoutActionMode == null ? View.VISIBLE : View.INVISIBLE);
            final int accentColor = getResources().getColor(R.color.accent);
            holder.itemView.setBackgroundColor(mPayoutSelector.selectedItems[position] ? accentColor : 0);
        }

        @Override
        public int getItemCount() {
            return players.length;
        }
    }

    private static class PlayerVH extends RecyclerView.ViewHolder{

        public ImageView iconIV;
        public TextView nameTV;
        public TextView cashTV;


        public Button foldBT;
        public Button callBT;



        public PlayerVH(View itemView) {
            super(itemView);

            iconIV = (ImageView)itemView.findViewById(R.id.icon);
            nameTV = (TextView)itemView.findViewById(R.id.name);
            cashTV = (TextView)itemView.findViewById(R.id.cash);

            foldBT = (Button)itemView.findViewById(R.id.fold_button);
            callBT = (Button)itemView.findViewById(R.id.call_button);
        }
    }

      @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.menu_game_payout,menu);
            mPayoutActionMode = mode;
            updatePayoutButton();
            mPlayerAdapter.notifyDataSetChanged();
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()){
                case R.id.action_payout:
                    payout();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mPayoutActionMode = null;
            mPayoutSelector.clear();
            mPlayerAdapter.notifyDataSetChanged();
            updatePayoutButton();
        }

}
