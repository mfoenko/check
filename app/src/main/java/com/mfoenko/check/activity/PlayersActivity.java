package com.mfoenko.check.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfoenko.check.R;
import com.mfoenko.check.data.Player;
import com.mfoenko.check.data.PlayersDatabase;
import com.mfoenko.check.dialog.EditPlayerDialogFragment;
import com.mfoenko.check.misc.MultiSelectAdapter;

public class PlayersActivity extends AppCompatActivity implements ActionMode.Callback {

    private PlayersDatabase mPlayersDatabase;

    private RecyclerView mRecyclerView;
    private PlayerAdapter mPlayerAdapter;

    private final static String EDIT_PLAYER_DIALOG_TAG = "edit";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players);

        mPlayersDatabase = PlayersDatabase.getInstance(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.players_recycler_view);
        mRecyclerView.setAdapter(mPlayerAdapter = new PlayerAdapter(mPlayersDatabase.getAllPlayers()));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_players, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_player:
                EditPlayerDialogFragment dialog = new EditPlayerDialogFragment();
                final Player player = new Player();
                dialog.setPlayer(player);
                dialog.setOnPositiveClickListener(new EditPlayerDialogFragment.OnPositiveClickListener() {
                    @Override
                    public void onPositiveClick(DialogInterface dialog, int which) {
                        mPlayersDatabase.insertPlayer(player);
                        mPlayerAdapter.setPlayers(mPlayersDatabase.getAllPlayers());
                        mPlayerAdapter.notifyDataSetChanged();

                    }
                });
                dialog.show(getFragmentManager(),EDIT_PLAYER_DIALOG_TAG);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class PlayerAdapter extends RecyclerView.Adapter<PlayerVH>  {

        private Player[] items;

        public MultiSelectAdapter mMultiSelectAdapter;


        public PlayerAdapter(Player... items) {
            this.items = items;
            this.mMultiSelectAdapter = new MultiSelectAdapter(items);
        }

        public void setPlayers(Player... players) {
            this.items = players;
            mMultiSelectAdapter.setItems(players);
        }

        ActionMode mActionMode = null;

        @Override
        public PlayerVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            View itemView = inflater.inflate(R.layout.player_list_item, viewGroup, false);
            final PlayerVH holder = new PlayerVH(itemView);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getAdapterPosition();
                    boolean isAnySelected = mMultiSelectAdapter.onItemClicked(pos);
                    if (isAnySelected && mActionMode == null) {
                        mActionMode = startActionMode(PlayersActivity.this);
                    } else {
                        if (!isAnySelected && mActionMode != null) {
                            mActionMode.finish();
                        }
                    }
                    if (mActionMode != null) {
                        mActionMode.invalidate();
                    }
                    notifyItemChanged(pos);
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(PlayerVH viewHolder, int i) {
            Player player = items[i];
            viewHolder.nameTV.setText(player.name);
            viewHolder.cashTV.setText(String.valueOf(player.cash));
            final int accentColor = getResources().getColor(R.color.accent);
            viewHolder.itemView.setBackgroundColor(mMultiSelectAdapter.selectedItems[i] ? accentColor : 0);

        }

        @Override
        public int getItemCount() {
            return items.length;
        }

    }


    private class PlayerVH extends RecyclerView.ViewHolder {

        public ImageView iconIV;
        public TextView nameTV;
        public TextView cashTV;


        public PlayerVH(View itemView) {
            super(itemView);


            iconIV = (ImageView) itemView.findViewById(R.id.icon);
            nameTV = (TextView) itemView.findViewById(R.id.name);
            cashTV = (TextView) itemView.findViewById(R.id.cash);


        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_players_selected, menu);
        return true;
    }



    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        menu.findItem(R.id.action_edit).setVisible(mPlayerAdapter.mMultiSelectAdapter.isOneSelected());
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_play:
                Player[] players = new Player[mPlayerAdapter.mMultiSelectAdapter.getNumSelected()];

                int curPos = 0;
                for (int i = 0; i < mPlayerAdapter.mMultiSelectAdapter.selectedItems.length; i++) {
                    if (mPlayerAdapter.mMultiSelectAdapter.selectedItems[i]) {
                        players[curPos] = mPlayerAdapter.items[i];
                        curPos++;
                    }
                }
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra(GameActivity.PLAYERS_KEY, players);
                startActivity(intent);
                mPlayerAdapter.mMultiSelectAdapter.clear();
                mode.finish();
                return true;

            case R.id.action_delete:
                for(int i=0;i<mPlayerAdapter.mMultiSelectAdapter.selectedItems.length;i++){
                    if(mPlayerAdapter.mMultiSelectAdapter.selectedItems[i]){
                        mPlayersDatabase.deletePlayer(mPlayerAdapter.items[i]);
                        mPlayerAdapter.notifyItemRemoved(i);
                    }
                }
                mPlayerAdapter.setPlayers(mPlayersDatabase.getAllPlayers());
                mode.finish();
                return true;

            case R.id.action_edit:
                Player tPlayer = null;

                for(int i=0;i<mPlayerAdapter.mMultiSelectAdapter.selectedItems.length;i++){
                    if(mPlayerAdapter.mMultiSelectAdapter.selectedItems[i]){
                        tPlayer = mPlayerAdapter.items[i];
                    }
                }
                final Player player = tPlayer;
                EditPlayerDialogFragment dialog = new EditPlayerDialogFragment();
                dialog.setPlayer(player);
                dialog.setOnPositiveClickListener(new EditPlayerDialogFragment.OnPositiveClickListener() {
                    @Override
                    public void onPositiveClick(DialogInterface dialog, int which) {
                        Log.i("AAA",player.toString());
                        mPlayersDatabase.updatePlayer(player);
                        mPlayerAdapter.notifyDataSetChanged();

                    }
                });
                dialog.show(getFragmentManager(),EDIT_PLAYER_DIALOG_TAG);
                return true;
        }

        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mPlayerAdapter.mMultiSelectAdapter.clear();
        mPlayerAdapter.notifyDataSetChanged();
        mPlayerAdapter.mActionMode = null;
    }



}
