package com.mfoenko.check.misc;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mfoenko.check.R;

/**
 * Created by mfoenko on 9/1/15.
 */
public class MultiSelectAdapter{

    public boolean[] selectedItems;

    public MultiSelectAdapter(Object[] objects){
        setItems(objects);
    }
    public void setItems(Object[] objects){

        selectedItems = new boolean[objects.length];
    }


    public void clear(){
        selectedItems = new boolean[selectedItems.length];
    }


    public boolean onItemClicked(int pos){
        selectedItems[pos] = !selectedItems[pos];

        return isAnySelected();
    }


    public int getNumSelected(){
        int count = 0;
        for(boolean b:selectedItems){
            count += (b?1:0);
        }
        return count;
    }

    public boolean isAnySelected(){
        for(boolean b:selectedItems){
            if(b){
                return true;
            }
        }
        return false;
    }

    public boolean isNoneSelected(){
        for(boolean b:selectedItems){
            if(b){
                return false;
            }
        }
        return true;
    }


    public boolean isOneSelected(){
        boolean one = false;
        for(boolean b:selectedItems){
            if(b && !one){
                one = true;
            }else if(b){
                return false;
            }
        }
        return one;
    }


}
