package com.mfoenko.check.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

/**
 * Created by mfoenko on 8/28/15.
 */
public class Player implements Parcelable {

    public long id;
    public String name;
    public int cash;
    public String iconLoc;
    public Bitmap icon;

    public Player(int id, String name, int cash, String iconLoc) {
        this.id = id;
        this.name = name;
        this.cash = cash;
        this.iconLoc = iconLoc;
    }
    public Player(){
        this.id = -1;
        this.cash = 0;
    }

    public Player(Player player){
        this.id = player.id;
        this.name = player.name;
        this.cash = player.cash;
        this.iconLoc = player.iconLoc;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Player>(){
        @Override
        public Player createFromParcel(Parcel source) {
            Player player = new Player();
            player.id = source.readLong();
            player.name = source.readString();
            player.cash = source.readInt();
            player.iconLoc = source.readString();

            return player;
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.cash);
        dest.writeString(this.iconLoc);
    }

    @Override
    public String toString() {
        return this.name + " " +this.cash;
    }

    public Bitmap getIcon(Context context){
        if(this.icon == null){
            loadIcon(context);
        }
        return this.icon;
    }

    public void loadIcon(Context context) {
        File dir = context.getExternalFilesDir("image");
        this.icon = BitmapFactory.decodeFile(dir.getAbsolutePath() + this.id+".png");
    }

    public boolean isInDatabase(){
        return id != -1;
    }
}
