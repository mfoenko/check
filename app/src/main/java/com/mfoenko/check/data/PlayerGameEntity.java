package com.mfoenko.check.data;

/**
 * Created by mfoenko on 9/1/15.
 */
public class PlayerGameEntity extends Player{

    public boolean isFolded = false;
    public boolean hasActed = false;
    public int wager = 0 ;

    public PlayerGameEntity(Player player){
        super(player);
    }

    public void fold(){
        isFolded = true;
        hasActed = true;
    }

    public void call(int amount){
        if(this.cash >= amount){
            this.wager = amount;
        }else {
            wager = this.cash;
        }
        hasActed = true;
    }

    public void reset(){
        isFolded = false;
        hasActed = false;
        wager = 0;
    }

}
