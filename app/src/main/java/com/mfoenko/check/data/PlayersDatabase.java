package com.mfoenko.check.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mfoenko on 8/28/15.
 */
public class PlayersDatabase extends SQLiteOpenHelper {

    public static final String DB_NAME = "players";
    public static final int DB_VER = 1;

    public static final String CREATE_STATEMENT = "CREATE TABLE players (_id integer primary key, name text, cash int, icon, text)";

    public static final String SELECT_ALL_PLAYERS_QUERY = "SELECT _id, name, cash, icon FROM players";
    public static final String INSERT_PLAYER_QUERY = "INSERT INTO players(name, cash, icon) VALUES (?, ?, ?)";
    public static final String REPLACE_PLAYER_QUERY = "INSERT OR REPLACE INTO players(name, cash, icon) VALUES (?, ?, ?)";
    public static final String UPDATE_PLAYER_QUERY = "UPDATE players SET name = ?, cash = ?, icon = ? WHERE _id=?";
    public static final String DELETE_PLAYER_QUERY = "DELETE FROM players WHERE _id = ?";

    private static PlayersDatabase instance;
    private SQLiteDatabase db;

    public static PlayersDatabase getInstance(Context context){
        if(instance == null){
            instance = new PlayersDatabase(context);
        }
        return instance;
    }

    private PlayersDatabase(Context context){
        super(context, DB_NAME, null, DB_VER);
        db = getWritableDatabase();
    }

    public Player insertPlayer(Player player){
        db.execSQL(INSERT_PLAYER_QUERY, new Object[]{player.name, player.cash, player.icon});

        //TODO: sql last index thing
        Cursor idCursor =  db.rawQuery("SELECT rowid FROM players", null);
        long playerID = -1;
        if(idCursor.moveToFirst()) {
            playerID = idCursor.getLong(0);
        }

        player.id = playerID;
        return player;
    }

    public void deletePlayer(Player player){
        db.execSQL(DELETE_PLAYER_QUERY, new Object[]{player.id});
    }

/*
    public Player getPlayer(long id){
        return null
    }*/

    public Player[] getAllPlayers(){
        Cursor allPlayersCursor = db.rawQuery(SELECT_ALL_PLAYERS_QUERY, null);
        /*
        *   SELECT id, name, cash, icon FROM players
        *   Cursor Contents:
        *   0: id
        *   1: name
        *   2: cash
        *   3: icon (iconLoc)
        * */
        Player[] players = new Player[allPlayersCursor.getCount()];
        for(int i=0;i<players.length;i++){
            allPlayersCursor.moveToNext();
            players[i] = new Player();
            players[i].id = allPlayersCursor.getLong(0);
            players[i].name = allPlayersCursor.getString(1);
            players[i].cash = allPlayersCursor.getInt(2);
            players[i].iconLoc = allPlayersCursor.getString(3);
        }
        return players;
    }

    public void updatePlayer(Player player){
        db.execSQL(UPDATE_PLAYER_QUERY, new Object[]{player.name, player.cash, player.icon, player.id});
    }
    public void updateOrReplacePlayer(Player player){
        db.execSQL(REPLACE_PLAYER_QUERY, new Object[]{player.name, player.cash, player.icon, player.id});
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
