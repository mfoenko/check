package com.mfoenko.check.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfoenko.check.R;
import com.mfoenko.check.data.Player;
import com.mfoenko.check.data.PlayersDatabase;

/**
 * Created by mfoenko on 9/10/15.
 */
public class EditPlayerDialogFragment extends DialogFragment {


    View mLayout;
    Player mPlayer;

    TextView mNameTV;
    TextView mCashTV;
    ImageView mIconIV;

    private OnPositiveClickListener mListener;

  /*  @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        playerDB = PlayersDatabase.getInstance(inflater.getContext());
        mLayout = inflater.inflate(R.layout.dialog_edit_player, null, false);
        ImageView iconIV = (ImageView)mLayout.findViewById(R.id.icon);
        iconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getImageIntent.setType("image*//*");
                v.getContext().startActivity(getImageIntent);
            }
        });
        return mLayout;
    }
*/
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mLayout = inflater.inflate(R.layout.dialog_edit_player, null, false);
        mNameTV = (TextView) mLayout.findViewById(R.id.name);
        mCashTV = (TextView) mLayout.findViewById(R.id.cash);
        mIconIV = (ImageView)mLayout.findViewById(R.id.icon);
        mIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getImageIntent.setType("image*//*");
                v.getContext().startActivity(getImageIntent);
            }
        });
        displayPlayer();
        builder.setView(mLayout);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPlayer.name = mNameTV.getText().toString();
                mPlayer.cash = Integer.valueOf(mCashTV.getText().toString());
                if(mListener != null){
                    mListener.onPositiveClick(dialog, which);
                }

            }
        })
                .setNegativeButton("Cancel", null);
        Dialog dialog = builder.create();
        dialog.requestWindowFeature(0);
        return dialog;
    }

    public void setOnPositiveClickListener(OnPositiveClickListener listener){
        mListener = listener;
    }

    public void displayPlayer(){
        if(mLayout != null && mPlayer != null) {
            mNameTV.setText(mPlayer.name);
            mCashTV.setText(String.valueOf(mPlayer.cash));
            // ((ImageView)mLayout.findViewById(R.id.icon)).setImage(mPlayer.getIon());
        }
    }

    public Player getPlayer(){
        mPlayer.name = ((TextView)mLayout.findViewById(R.id.name)).getText().toString();
        mPlayer.cash = Integer.parseInt(((TextView)mLayout.findViewById(R.id.cash)).getText().toString());

        return mPlayer;
    }

    public void setPlayer(Player player){
        this.mPlayer = player;
        displayPlayer();
    }

    public static interface OnPositiveClickListener{
        public void onPositiveClick(DialogInterface dialog, int which);
    }
}
